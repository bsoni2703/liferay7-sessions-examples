<%@ include file="/init.jsp" %>

<liferay-ddm:template-renderer
    className="<%= Product.class.getName() %>"
    contextObjects="<%= new HashMap<String, Object>() %>"
    displayStyle="<%= displayStyle %>"
    displayStyleGroupId="<%= displayStyleGroupId %>"
    entries="${products}"
>
	<c:if test="${not empty products}">
	    <aui:row>
	    	<aui:col md="6"><b>Name:</b></aui:col>
	    	<aui:col md="6">${products.get(0).getName() }</aui:col>
	    </aui:row>
	    <aui:row>
	    	<aui:col md="6"><b>Price:</b></aui:col>
	    	<aui:col md="6">${products.get(0).getPrice() }</aui:col>
	    </aui:row>
	    <aui:row>
	    	<aui:col md="6"><b>Category:</b></aui:col>
	    	<aui:col md="6">${products.get(0).getCategory() }</aui:col>
	    </aui:row>
	    <aui:row>
	    	<aui:col md="6"><b>Description:</b></aui:col>
	    	<aui:col md="6">${products.get(0).getDescription() }</aui:col>
	    </aui:row>
	</c:if>
	<c:if test="${empty products}">
		No products Available.
	</c:if>
</liferay-ddm:template-renderer>
