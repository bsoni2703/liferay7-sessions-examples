/**
 * Copyright 2000-present Liferay, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ch5finland.genelec.site.portlet;

import java.util.Locale;
import java.util.Map;

import org.osgi.service.component.annotations.Component;

import com.ch5finland.genelec.site.bean.Product;
import com.ch5finland.genelec.site.constants.GenelecProductInfoPortletKeys;
import com.liferay.portal.kernel.portletdisplaytemplate.BasePortletDisplayTemplateHandler;
import com.liferay.portal.kernel.template.TemplateHandler;
import com.liferay.portal.kernel.template.TemplateVariableGroup;

/**
 * @author Mahipalsinh Rana 
 * GenelecProductDisplayTemplateHandler to handle the Product ADT
 *
 */
@Component(
	immediate = true,
	property = {
		"javax.portlet.name=" + GenelecProductInfoPortletKeys.GenelecProductInfoPortlet,
    },
    service = TemplateHandler.class
)
public class GenelecProductDisplayTemplateHandler extends BasePortletDisplayTemplateHandler {

	@Override
	public String getClassName() {
		return Product.class.getName();
	}

	@Override
	public String getName(Locale arg0) {
		return GenelecProductInfoPortletKeys.PRODUCT_TEMPLATE;
	}

	@Override
	public String getResourceName() {
		return GenelecProductInfoPortletKeys.GenelecProductInfoPortlet;
	}

	@Override
	public Map<String, TemplateVariableGroup> getTemplateVariableGroups(long classPK, String language, Locale locale)
			throws Exception {

		Map<String, TemplateVariableGroup> templateVariableGroups = super.getTemplateVariableGroups(classPK, language,
				locale);

		TemplateVariableGroup templateVariableGroup = templateVariableGroups.get("fields");

		templateVariableGroup.empty();
		// add product field into the templateVariableGroup
		templateVariableGroup.addVariable("product", Product.class, "entry");

		return templateVariableGroups;
	}
}