package com.ch5finland.genelec.site.constants;

/**
 * @author Mahipalsinh Rana
 */
public class GenelecProductInfoPortletKeys {

	public static final String GenelecProductInfoPortlet = "GenelecProductInfoPortlet";
	public static final String PRODUCT_TEMPLATE = "Product Template";

}