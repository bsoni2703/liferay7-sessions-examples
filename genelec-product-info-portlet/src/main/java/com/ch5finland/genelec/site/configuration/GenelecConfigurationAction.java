package com.ch5finland.genelec.site.configuration;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Reference;

import com.ch5finland.genelec.site.constants.GenelecProductInfoPortletKeys;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.ConfigurationAction;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;

/**
 * @author Mahipalsinh Rana GenelecConfigurationAction to handle the portlet
 *         configuration
 *
 */
@Component(configurationPolicy = ConfigurationPolicy.OPTIONAL, 
	immediate = true, 
	property = { "javax.portlet.name=" + GenelecProductInfoPortletKeys.GenelecProductInfoPortlet 
		}, 
	service = ConfigurationAction.class)
public class GenelecConfigurationAction extends DefaultConfigurationAction {
	private static final Log _log = LogFactoryUtil.getLog(GenelecConfigurationAction.class.getName());

	@Override
	public String getJspPath(HttpServletRequest request) {
		_log.debug("getJspPath Called");
		return "/configuration.jsp";
	}

	/**
	 * setServletContext: Sets the servlet context, use your portlet's bnd.bnd
	 * Bundle-SymbolicName value.
	 * 
	 * @param servletContext
	 *            The servlet context to use.
	 */

	@Override
	@Reference(target = "(osgi.web.symbolicname=com.ch5finland.genelec.site)", unbind = "-")
	public void setServletContext(ServletContext servletContext) {
		super.setServletContext(servletContext);
	}
}
