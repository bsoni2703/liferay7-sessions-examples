package com.ch5finland.genelec.site.portlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;

import com.ch5finland.genelec.site.bean.Product;
import com.ch5finland.genelec.site.constants.GenelecProductInfoPortletKeys;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;

/**
 * @author Mahipalsinh Rana
 * GenelecProductInfoPortlet
 */
@Component(
	immediate = true, 
	configurationPid = "com.ch5finland.genelec.site.portlet.GenelecConfiguration", 
	property = {
		"com.liferay.portlet.display-category=category.development", 
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=Genelec Product", 
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.init-param.configuration-template=/configuration.jsp",
		"javax.portlet.name=" + GenelecProductInfoPortletKeys.GenelecProductInfoPortlet,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user" 
		}, 
	service = Portlet.class)
public class GenelecProductInfoPortlet extends MVCPortlet {
	private static final Log _log = LogFactoryUtil.getLog(GenelecProductInfoPortlet.class.getName());

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		_log.info("Excuting render method");
		// Product object will be set here
		Product product = new Product();
		product.setName("HP-Laptop");
		product.setDescription("Laptop for developer");
		product.setPrice(50000);
		product.setCategory("Electronics");
		List<Product> products = new ArrayList<Product>();
		products.add(product);
		// add product entries
		renderRequest.setAttribute("products", products);
		super.render(renderRequest, renderResponse);
	}
}