**This repositiry contains all the sample examples for Liferay 7 GA7 portal.**

Following are the list of projects.

1. Sample Hello World MVC Portlet: **hello-world** in Liferay gradle workspace **sessions-workspace**.
2. Service Builder: **basic/basic-api** and **basic/basic-service** in Liferay gradle workspace **sessions-workspace**.
3. MVC Portlet with CRUD: **basic-web** in Liferay gradle workspace **sessions-workspace**.
4. Login Fragment Hook: **login-fragment** in Liferay gradle workspace **sessions-workspace**.
5. User Service Wrapper and Language Hook Module: **serviceWrappersHook** in Liferay gradle workspace **sessions-workspace**.
6. Application Display Content(ADT) for Custom Application: **/genelec-product-info-portlet** maven project.
7. CustomJSPBag to override portal core jsps: **core-jsp-override** in Liferay gradle workspace **sessions-workspace**.
8. Liferay Page Layout: **sample-layout**
9. Liferay Theme Contributor: **Demo-theme-contributor** in Liferay gradle workspace **sessions-workspace**.
10. Liferay Theme: **session-theme**
11. EXT Plugin Example: **sample-ext** (Do not deploy the sample, as it may break your existing functionalites.)

