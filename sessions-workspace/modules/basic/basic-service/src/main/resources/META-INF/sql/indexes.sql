create index IX_FF9C5503 on sample_Foo (field2);
create index IX_29E5424B on sample_Foo (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_DBDFDF8D on sample_Foo (uuid_[$COLUMN_LENGTH:75$], groupId);